# Pquetes, Clases

- Buena practica el nombre de paquetes con **guiones bajos.**
- El nombre de clases empezado por **mayuscula.**

```java

package funciones_basicas;

private class Operaciones {
    public static int suma ( int a, int b ) { return a + b; }
    public static int mult ( int a, int b ) { return a * b; }
    public static int div  ( int a, int b ) { return a / b; }

}

```

1. Primero se declara el nombre del paquete.
2. A continuación se importan todos los paquetes/clases.
3. Se declaran todas las clases.

```java

package general;

import funciones_basicas.Operaciones;

public class Main {                               // Clase Main.

    public static int main ( String[] args ) {    // Metodo main.
        int num_1 = 3,
            num_2 = 5,
            res;

        res = Operaciones.suma ( num_1, num_2 );  // res = 8.
        res = Operaciones.mult ( num_1, num_2 );  // res = 15.
        res = Operaciones.div  ( num_2, num_1 );  // res = 1.
    }

}

```

