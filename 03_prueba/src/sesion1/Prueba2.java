package sesion1;

public class Prueba2 {

    public static void main(String[] args) {

        // Prefiero declarar todo arriba
        // y después definir.

        int resultado;
        String fragmentoNombreAlumnos = "Victor",
               nombreAlumnos[] =
               {
                   "Antonio",
                   "Marta",
                   "Victor",
                   "Hugo",
                   "David",
                   "Victor"
               };

        resultado = contarUsuarios ( fragmentoNombreAlumnos, nombreAlumnos );
        System.out.println ( "Total resultados: " + resultado );
    }

    static int contarUsuarios ( String fragmentoNombreAlumno, String[] nombreAlumnos ) {

        int totalEncontrados = 0;

        // Guardo el numero total de palabras en 'tot'
        int tot = nombreAlumnos.length;


        // No pongo corchetes para que quede más legible el codigo.
        // For, If... siempre ejecutan una linea por debajo suya o un
        // bloque de instrucciones.

        // Recorro el bucle hasta que i sea menor que el total de palabras
        // y accedo a la palabra i'esima del array para compararla con
        // la palabra 'Victor' en este caso, si es igual incremento totalEncontrados.
        for ( int i=0; i<tot; i++ )
            if ( nombreAlumnos[i] == fragmentoNombreAlumno )
                totalEncontrados++;

        return totalEncontrados;
    }

}
