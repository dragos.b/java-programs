package sesion1;
import java.util.Scanner;
public class ecuacion {

    public static void print_nums( double consts[], int a, int b, int c ) {
        System.out.printf( "Vas a calcular: %.0f𝝒² + %.0f𝝒  + %.0f = 0\n¿Calcular?: [true/false]\n\n",
                consts[a],
                consts[b],
                consts[c] );
    }
    public static void main ( String[] args ) {
        /*
         * ax² + bx + c = 0
         */
        Scanner sc = new Scanner ( System.in );
        boolean keep = false;
        String letter[] = { "𝓪", "𝓫", "𝓬" };
        final int a = 0,
                  b = 1,
                  c = 2;
                     //     a  b  c
        double consts[] = { 0, 0, 0 };
        double res_x1, res_x2,
               denominator, frac_res;

        // Entrada de a, b y c.
        // datos[a], datos[b], datos[c].
        while ( !keep ) {
            for ( int i=0; i<consts.length; i++ ) {
                System.out.print ( letter[i] + ": " );
                consts[i] = sc.nextDouble();
            }
            print_nums ( consts, a, b, c );
            keep = sc.nextBoolean();
        }


        // Calculos
        frac_res = Math.pow (consts[b], 2) - 4 * consts[a] * consts[c];
        denominator = 2 * consts[a];

        if ( frac_res > 0 ) {		                    // Discriminante > 0
            res_x1 = (-consts[b] + Math.sqrt(frac_res) ) / denominator;
            res_x2 = (-consts[b] - Math.sqrt(frac_res) ) / denominator;
            System.out.printf ( "𝘟₁: %.3f \n𝘟₂: %.3f\n" , res_x1, res_x2 );

        } else 	// Discriminante < 0 o = 0
            System.out.printf ( "Sin Resultado\n\nNo se puede calcular  √%.0f\n", frac_res );

        sc.close();
    }
}
