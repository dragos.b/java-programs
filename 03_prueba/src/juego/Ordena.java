package juego;

public class Ordena {
    public static void ordenaLetras ( String letras ) {
        char      letrasN[] = letras.toCharArray ();   // Creo un array char que guarda el parametro 'letras'.
        Character let1, let2;                          // Dos objetos tipo Character para utilizar el metodo
                                                       // 'compareTo()'.
        int       pasadas   = 0,                       // Pasadas para while.
                rvcompare,                             // Return Value compareTo.
                size = letras.length ();

        // Algoritmo de ordenación.
        while ( pasadas <= size ) {
            for ( int i = 0; i < size - 1; i++ ) {
                let1 = letrasN[i];     // Letra i-esima.
                let2 = letrasN[i + 1]; // Letra i+1-esima.

                // Si let1 > let2, let1 está mas cerca de la 'z'.
                rvcompare = let1.compareTo ( let2 );

                // rvcompare > 0? entonces let1>let2 en ASCII.
                // a = 97; b = 98 b.compareTo(a) == 1.
                if ( rvcompare > 0 ) {
                    letrasN[i + 1] = let1;
                    letrasN[i]     = let2;
                }
            }
            pasadas++;
        }
        // Imprime la cadena ordenanda.
        for ( int i = 0; i < size; i++ )
            System.out.print ( letrasN[i] );
        System.out.println ();
    }
}