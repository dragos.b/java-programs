public class divisiones {

	public static void main(String[] args) {
		int numA = 10;
		int numB = 25;
		int div;
		// Operador Ternario
		div = (numA > numB)? numA/numB: numB/numA;
		System.out.println("Resultado de la division: " + div);
		
		numA <<= 1;
		System.out.print("numA desplazado un bit <<\n");
		System.out.print(numA);
				
	}

}
