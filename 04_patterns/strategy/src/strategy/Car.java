package strategy;

public class Car extends Vehicle
{
    public Car ( String name )
    {
        super( name );
        moveType = new MoveCar();
    }

    public void moveCar ()
    {
        moveType.move();
    }

}
